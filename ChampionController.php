<?php
use Carbon\Carbon;

class ChampionController
{
    public function display_champions()
    {
    	// get information
    	$champion_id = Input::get('champion_id');
    	$api_key = Input::get('api_key');
    	$preview_date = Input::get('preview_date');
    	$eight_pm = Input::get('eight_pm');
    	$display_filename = Input::get('display_filename');

    	// set these variables to false by default
    	$eight_pm_enabled = FALSE;
    	$display_filename_enabled = FALSE;

    	// date stuff
    	$today = Carbon::now();
        $yesterday = Carbon::yesterday();

    	// this should only work on STAGE
        if (stristr(gethostname(), 'stage') !== FALSE)
        {
        	// The api key is not blank
	        if($api_key != '')
	        {
	        	// does it match?
	        	if($api_key == Config::get('app.champion_api_key'))
	        	{
	        		if($display_filename == TRUE)
	        		{
	        			$display_filename_enabled = TRUE;
	        		}

	        		if($eight_pm == TRUE)
	        		{
		        		// set the eight pm variable
		        		$eight_pm_enabled = TRUE;
		        	}

	        		// preview date is present.
	        		if($preview_date != '')
	        		{
	        			// set the preeview date only if its a vaild date.
						$date = date_parse_from_format("Y-m-d", $preview_date);
						if (checkdate($date['month'], $date['day'], $date['year'])) {
						   $today = Carbon::createFromFormat('Y-m-d', $preview_date);
						}
						else
						{
							return Response::json(array(
				                'status' => 'error',
				                'errors' => 'Invalid Date',
				                'data' => array()
				            ));
						}
	        		}
	        	}
	        	else // bad API Key
	        	{
	        		return Response::json(array(
		                'status' => 'error',
		                'errors' => 'Invalid Api Key',
		                'data' => array()
		            ));
	        	}
	        }
	    }

	   	// date variables
	    $today8pm = $today->copy()->hour(20)->minute(0)->second(0);
        $monday = $today->copy()->startOfWeek();
        $next_monday = $today->copy()->next()->startOfWeek();
        $next_monday_file = $next_monday->toDateString().".json";
        $today_ymd = $today->toDateString();

        // a champion ID is required
    	if($champion_id == '')
    	{
    		return Response::json(array(
	            'status' => 'error',
	            'errors' => 'champion id is required',
	            'data' => array()
	        ));
    	}
    	else
    	{
    		// get the champion json path
    		$champion_path = public_path()."/toc/".$champion_id;

    		// does this folder exist?
    		if(file_exists($champion_path))
    		{
    			// get all the files and put them in an array
    			$json_files_in_arary = scandir($champion_path."/");

    			// Todays files
    			$todays_file = $champion_path."/".$today_ymd.".json";
    			$todays_file_8pm = $champion_path."/".$today_ymd."_8pm.json";

    			// Does todays Main file or todays 8pm file exist?
    			if(file_exists($todays_file) || file_exists($todays_file_8pm))
    			{
    				// is it 8pm / past 8pm (or are we manually testing after 8pm)?
		    		if ((!$today8pm->isFuture()) || ($eight_pm_enabled == TRUE))
		    		{
		    			// if todays 8pm file exists
		    			if(file_exists($todays_file_8pm))
		    			{
		    				$file = $today_ymd."_8pm.json";
		    			}
		    			else
		    			{
	    					// is todays date the last file in the folder requested after 8pm?...
	    					// if it is, dont load any new file
	    					$last_file = end($json_files_in_arary);

	    					if($last_file == $today_ymd.".json")
	    					{
	    						$file = $last_file;
	    					}
	    					else
	    					{
	    						// get the file for next day since its past 8pm
		    					$file = date('Y-m-d', strtotime("+1 day", strtotime($today_ymd)));
		    					$file = $file.".json";

		    					// for fridays
		    					if(!file_exists($champion_path."/".$file))
		    					{
		    						$file = $this->check_for_current_json($json_files_in_arary, $champion_path, $next_monday);
		    					}

		    				}
		    			}
		    		}
		    		else
		    		{
		    			// its not past 8pm or we arent testing past 8pm on a given day,
		    			// just show todays file.
    					$file = $today_ymd.".json";
		    		}

    			}
    			else // todays file doesnt exist... its probably a weekend or past date
    			{
    				// if its the weekend then show last fridays file
    				if($today->isWeekend())
	    			{
	    				if($today->dayOfWeek == Carbon::SATURDAY)
	    				{
		    				$following_monday = '+2 days';
	    				}
	    				if($today->dayOfWeek == Carbon::SUNDAY)
	    				{
	    					$following_monday = '+1 day';
	    				}

	    				$the_monday = date('Y-m-d', strtotime($following_monday, strtotime($today_ymd)));
	    				// a friday at 8pm file
	    				$following_json_file_8pm = $the_monday.'_8pm';
	    				$following_json_json = $the_monday;

	    				// does an 8pm file exist for the last friday?
	    				if(file_exists($champion_path."/".$following_json_file_8pm.'.json'))
	    				{
	    					$file = $this->check_for_current_json($json_files_in_arary, $champion_path, $following_json_file_8pm);
	    				}
	    				else // regular friday json
	    				{
	    					$file = $this->check_for_current_json($json_files_in_arary, $champion_path, $following_json_json);
	    				}
	    			}
	    			else
	    			{
	    				// checks if a last file exists.
						$file = $this->check_for_current_json($json_files_in_arary, $champion_path, $today_ymd);
	    			}
    			}

    			// just for testing purposes, only if on stage, correct api key and parameter is present
    			if($display_filename_enabled == TRUE)
    			{
    				echo $file;
    			}
    			// get the file contents
    			$string = file_get_contents($champion_path."/".$file);

    			try {

	    			// decoded json
					$decoded_json = json_decode($string, true);

	    			return Response::json(array(
			            'status' => 'ok',
			            'errors' => array(),
			            'data' => $decoded_json
			        ));

			    } catch(Exception $e) {

			    	return Response::json(array(
			            'status' => 'error',
			            'errors' => array('message' => 'Bad Json'),
			            'data' => array()
			        ));

			    }

    		}
    		else
    		{
    			return Response::json(array(
		            'status' => 'error',
		            'errors' => 'Folder does not exist',
		            'data' => array()
		        ));
    		}
    	}
    }

    public function check_for_current_json($folder_array, $path, $date)
    {

    	$file = $date.'.json';

    	if(!file_exists($path."/".$file))
		{
			$the_file = end($folder_array);
		}
		else //file exists, return that file
		{
			$the_file = $file;
		}

		return $the_file;
	}
}